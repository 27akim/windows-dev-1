﻿using System.Collections.Generic;
using System.Windows;
using MahApps.Metro.Controls;

namespace Win10task1.View
{
    /// <summary>
    /// Логика взаимодействия для AddUserWindow.xaml
    /// </summary>
    public partial class AddUserWindow : MetroWindow
    {
        public AddUserWindow(List<User> users)
        {
            InitializeComponent();
            DataContext = new AddUserWindowViewModel(this, users);
        }
    }
}
