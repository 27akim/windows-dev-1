﻿using System;
using System.Windows;
using MahApps.Metro.Controls;

namespace Win10task1.View
{
    /// <summary>
    /// Логика взаимодействия для CreateProcessWindow.xaml
    /// </summary>
    public partial class CreateProcessWindow : MetroWindow
    {
        public CreateProcessWindow()
        {
            InitializeComponent();
        }

        private void Create_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
