﻿using System.Windows;
using MahApps.Metro.Controls;

namespace Win10task1.View
{
    /// <summary>
    /// Логика взаимодействия для AddIssueWindow.xaml
    /// </summary>
    public partial class AddIssueWindow : MetroWindow
    {
        public AddIssueWindow()
        {
            InitializeComponent();
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
