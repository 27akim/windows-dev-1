﻿using System;
using System.ComponentModel;
using System.Windows;
using MahApps.Metro.Controls;

namespace Win10task1.View
{
    /// <summary>
    /// Логика взаимодействия для Window1.xaml
    /// </summary>
    public partial class LoginWindow : MetroWindow
    {
        public bool createNewAcc;

        public LoginWindow()
        {
            createNewAcc = false;
            InitializeComponent();
            DataContext = new LoginWindowViewModel(this);
        }

        private void CreateAcc_Click(object sender, RoutedEventArgs e)
        {
            createNewAcc = true;
            this.DialogResult = true;
        }
    }
}
