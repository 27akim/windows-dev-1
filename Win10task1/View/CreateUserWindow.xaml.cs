﻿using System.Windows;
using MahApps.Metro.Controls;

namespace Win10task1.View
{
    /// <summary>
    /// Логика взаимодействия для CreateUserWindow.xaml
    /// </summary>
    public partial class CreateUserWindow : MetroWindow
    {
        public CreateUserWindow()
        {
            InitializeComponent();
            DataContext = new CreateUserWindowViewModel(this);
        }
    }
}
