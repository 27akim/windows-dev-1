﻿using System;
using System.Threading.Tasks;

namespace Win10task1
{
    public interface IDbInterface<T> : IDisposable
        where T : class
    {
        Task CreateAsync(T entity);
        Task DeleteAsync(T entity);
        Task UpdateAsync(T entity);
        Task SaveChangesAsync();
    }
}
