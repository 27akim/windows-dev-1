﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Win10task1
{
    public class PriorityToIssueBackgroundConverter : IValueConverter
    {
        private static readonly Brush HighPriority = new SolidColorBrush(Colors.Red);
        private static readonly Brush MediumPriority = new SolidColorBrush(Colors.Orange);
        private static readonly Brush LowPriority = new SolidColorBrush(Colors.Green);

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || !(value is String))
            {
                return value;
            }
            if((String)value == "High")
            {
                return HighPriority;
            }
            else if ((String)value == "Medium")
            {
                return MediumPriority;
            }
            else if ((String)value == "Low")
            {
                return LowPriority;
            }
            else
            {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
