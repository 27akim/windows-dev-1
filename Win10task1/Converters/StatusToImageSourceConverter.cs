﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Win10task1
{
    public class StatusToImageSourceConverter : IValueConverter
    {
        private static readonly string toDoWay = "pack://application:,,,/Resources/Icons/to_do.png";
        private static readonly string inProgressWay = "pack://application:,,,/Resources/Icons/in_progress.png";
        private static readonly string inReview = "pack://application:,,,/Resources/Icons/in_review.png";
        private static readonly string done = "pack://application:,,,/Resources/Icons/done.png";

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || !(value is String))
            {
                return value;
            }
            if ((String)value == "To Do")
            {
                return toDoWay;
            }
            else if ((String)value == "In Progress")
            {
                return inProgressWay;
            }
            else if ((String)value == "In Review")
            {
                return inReview;
            }
            else if ((String)value == "Done")
            {
                return done;
            }
            else
            {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
