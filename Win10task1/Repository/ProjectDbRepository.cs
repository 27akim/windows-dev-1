﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;

namespace Win10task1
{
    class ProjectDbRepository : IDbInterface<Process>, IDbInterface<User>, IDbInterface<Issue>
    {
        private ProjectContext _context;

        static object locker = new object();

        public ProjectDbRepository()
        {
            this._context = new ProjectContext();
        }

        public async Task CreateAsync(Process p)
        {
            await Task.Run(() => _context.Processes.Add(p));
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Process p)
        {
            p = await _context.Processes.Where(c => c.Id == p.Id).FirstOrDefaultAsync();
            //remove connected users exept creator
            foreach (User u in p.Users.ToList())
            {
                if (u.GetType().IsSubclassOf(typeof(User)))
                {
                    await DeleteAsync(u);
                }
            }
            //remove connected issues
            foreach (Issue i in p.Issues.ToList())
            {
                await DeleteAsync(i);
            }
            _context.Processes.Remove(p);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(Process p)
        {
            await Task.Run(() =>_context.Entry(p).State = EntityState.Modified);
            await _context.SaveChangesAsync();
        }

        public async Task<string> GetUserTypeAsync(Process p, User u)
        {
            return await Task.Run(() =>
            {
                foreach (Process x in _context.Processes.Include(t => t.Users))
                {
                    if (x.Id == p.Id)
                    {
                        return x.Users.Where(y => y.Login == u.Login).First().GetType().Name;
                    }
                }
                return null;
            });
        }

        public async Task<List<User>> GetProcessUsersAsync(Process p)
        {
            List<User> list = new List<User>();
            p = await _context.Processes.Where(x => x.Id == p.Id).FirstAsync();
            await Task.Run(() => _context.Entry(p).Collection(c => c.Users).Load());
            if (p != null && p.Users != null)
            {
                foreach (User u in p.Users)
                {
                    list.Add(u);
                }
                return list;
            }
            return null;
        }

        public async Task<List<Issue>> GetProcessIssuesAsync(Process p)
        {
            List<Issue> list = new List<Issue>();
            p = await _context.Processes.Where(x => x.Id == p.Id).FirstAsync();
            await Task.Run(() => _context.Entry(p).Collection(c => c.Issues).Load());           
            if (p != null && p.Issues != null)
            {
                foreach (Issue i in p.Issues)
                {
                    list.Add(i);
                }
                return list;
            }
            return null;
        }

        public async Task AddUserAsync(User u, Process p)
        {
            await Task.Run(() => _context.Processes.Where(x => x.Id == p.Id).First().Users.Add(u));
            await _context.SaveChangesAsync();
        }


        public async Task AddIssueAsync(Issue i, Process p)
        { 
            await Task.Run(() => _context.Processes.Where(x => x.Id == p.Id).First().Issues.Add(i));
            await _context.SaveChangesAsync();
        }


        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public async Task CreateAsync(Issue i)
        {
            await Task.Run(() => _context.Issues.Add(i));
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Issue i)
        {
            await Task.Run(() => _context.Issues.Remove(_context.Issues.Find(i.Id)));
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(Issue i)
        {
            await Task.Run(() => _context.Entry(i).State = EntityState.Modified);
            await _context.SaveChangesAsync();
        }

        public async Task CreateAsync(User u)
        {
            await Task.Run(() => _context.Users.Add(u));
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(User u)
        {
            await Task.Run(() => _context.Users.Remove(_context.Users.Find(u.Id)));
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(User u)
        {
            await Task.Run(() => _context.Entry(u).State = EntityState.Modified);
            await _context.SaveChangesAsync();
        }

        public async Task<User> GetUserByLoginAsync(string login)
        {
            return await _context.Users.Where(x => x.Login == login).FirstAsync();
        }

        public async Task<List<Process>> GetUserProcessesAsync(User u)
        {
            List<Process> list = new List<Process>();
            await Task.Run(() =>
            {
                foreach (Process p in _context.Processes.Include(t => t.Users))
                {
                    foreach (User x in p.Users)
                    {
                        if (x.Login == u.Login)
                        {
                            list.Add(p);
                        }
                    }
                }
            });
            await _context.SaveChangesAsync();
            return list;
        }

        public async Task AddProcessAsync(User u, Process p)
        {
            await Task.Run(() => _context.Users.Where(x => x.Id == u.Id).First().Processes.Add(p));
            await _context.SaveChangesAsync();
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
