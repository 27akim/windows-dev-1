﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Win10task1
{
    public class Worker : User
    {
        public Worker()
        { }

        public Worker(string loginValue, string nameValue, string surnameValue, string passwordValue)
            : base(loginValue, nameValue, surnameValue, passwordValue)
        {

        }
    }
}
