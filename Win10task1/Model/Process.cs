﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Win10task1
{
    public class Process : INotifyPropertyChanged
    {
        private DateTime _date;
        private string _name;
        private string _creator;
        public ICollection<User> Users { get; set; }
        public ICollection<Issue> Issues { get; set; }

        public int Id { get; set; }
        public DateTime Date
        {
            get { return _date; }
            set
            {
                _date = value;
                OnPropertyChanged(nameof(Date));
            }
        }
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        public string Creator
        {
            get { return _creator; }
            set
            {
                _creator = value;
                OnPropertyChanged(nameof(Creator));
            }
        }

        public Process()
        {
            Users = new List<User>();
            Issues = new List<Issue>();
        }

        public Process(string nameValue, DateTime dateValue, User userValue)
        {
            Users = new List<User>();
            Issues = new List<Issue>();
            _name = nameValue;
            _date = dateValue;
            _creator = userValue.Login;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
