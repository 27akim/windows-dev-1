﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Win10task1
{
    public class Issue : INotifyPropertyChanged
    {
        private string _name;
        private DateTime _date;
        private string _priority;
        private string _status;
        private string _description;

        public int Id { get; set; }
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged(nameof(Name));
            }
        }
        public DateTime Date
        {
            get { return _date; }
            set
            {
                _date = value;
                OnPropertyChanged(nameof(Date));
            }
        }
        public string Priority
        {
            get { return _priority; }
            set
            {
                _priority = value;
                OnPropertyChanged(nameof(Priority));
            }
        }
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                OnPropertyChanged(nameof(Status));
            }
        }
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                OnPropertyChanged(nameof(Description));
            }
        }
        public ICollection<Process> Processes { get; set; }

        public Issue()
        {

        }

        public Issue(string nameValue, DateTime dateValue, string priorityValue, string statusValue, string descriptionValue)
        {
            _name = nameValue;
            _date = dateValue;
            _priority = priorityValue;
            _status = statusValue;
            _description = descriptionValue;
            Processes = new List<Process>();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
