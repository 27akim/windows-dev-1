﻿using System;

namespace Win10task1
{
    public class Maintainer : User
    {
        public Maintainer()
        { }

        public Maintainer(string loginValue, string nameValue, string surnameValue, string passwordValue)
            : base(loginValue, nameValue, surnameValue, passwordValue)
        {

        }
    }
}
