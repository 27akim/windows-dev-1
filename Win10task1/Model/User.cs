﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;

namespace Win10task1
{
    public class User : INotifyPropertyChanged
    {
        private string _login;
        private string _name;
        private string _surname;
        private string _password;
        public int Id { get; set; }
        public string Login
        {
            get { return _login; }
            set
            {
                _login = value;
                OnPropertyChanged(nameof(Login));
            }
        }
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged(nameof(Name));
            }
        }
        public string Surname
        {
            get { return _surname; }
            set
            {
                _surname = value;
                OnPropertyChanged(nameof(Surname));
            }
        }

        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                OnPropertyChanged(nameof(Password));
            }
        }

        public ICollection<Process> Processes { get; set; }

        public User()
        {
            Processes = new List<Process>();
        }

        public User(string loginValue,  string nameValue, string surnameValue, string passwordValue)
        {
            _login = loginValue;
            _name = nameValue;
            _surname = surnameValue;
            _password = passwordValue;
            Processes = new List<Process>();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
