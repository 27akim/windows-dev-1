﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Win10task1
{
    public class Guest : User
    {
        public Guest()
        { }

        public Guest(string loginValue, string nameValue, string surnameValue, string passwordValue)
            : base(loginValue, nameValue, surnameValue, passwordValue)
        {

        }
    }
}
