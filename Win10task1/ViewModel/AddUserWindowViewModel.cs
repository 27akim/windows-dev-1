﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using MVVM;

namespace Win10task1
{
    public class AddUserWindowViewModel : INotifyPropertyChanged
    {
        private Window _currentWindow;
        private List<User> _users;

        public AddUserWindowViewModel(Window window, List<User> users)
        {
            _currentWindow = window;
            _users = users;
        }

        public async void CheckInput()
        {
            await Task.Run(() =>
            {
                using (ProjectContext db = new ProjectContext())
                {
                    if (String.IsNullOrEmpty(Login))
                    {
                        Errors = "Login is empty";
                    }
                    else if (db.Users.Where(p => p.Login == Login).Count() != 0)
                    {
                        foreach (User u in _users)
                        {
                            if (u.Login == Login)
                            {
                                Errors = "User is already in project";
                                return;
                            }
                        }
                        _currentWindow.Dispatcher.Invoke(() => { _currentWindow.DialogResult = true; });
                    }
                    else
                    {
                        Errors = "No such user";
                    }
                }
            });
        }

        private RelayCommand _addUserCommand;
        public  RelayCommand AddUserCommand
        {
            get
            {
                return _addUserCommand ??
                    (_addUserCommand = new RelayCommand(obj =>
                    {
                        CheckInput();
                    }));
            }
        }

        private string _errors;
        public string Errors
        {
            get => _errors;
            set
            {
                _errors = value;
                OnPropertyChanged(nameof(Errors));
            }
        }

        private string _login;
        public string Login
        {
            get => _login;
            set
            {
                _login = value;
                OnPropertyChanged(nameof(Login));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
