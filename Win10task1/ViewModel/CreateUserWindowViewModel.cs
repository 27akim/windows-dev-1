﻿using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using MVVM;
using System.Threading.Tasks;
using System;
using NLog;

namespace Win10task1
{
    public class CreateUserWindowViewModel : INotifyPropertyChanged
    {
        private Window _currentWindow;
        private Visibility _loaderVisibility;
        private Visibility _creatingVisibility;
        private Logger _logger;

        public CreateUserWindowViewModel(Window window)
        {
            _currentWindow = window;
            _logger = LogManager.GetCurrentClassLogger();
            LoaderVisibility = Visibility.Hidden;
            CreatingVisibility = Visibility.Visible;
        }

        private RelayCommand _createCommand;
        public RelayCommand CreateCommand
        {
            get
            {
                return _createCommand ??
                    (_createCommand = new RelayCommand(async (obj) =>
                    {
                        await Task.Run(() =>
                        {
                            CheckInput();
                        });
                    }));
            }
        }

        private RelayCommand _cancelCommand;
        public RelayCommand CancelCommand
        {
            get
            {
                return _cancelCommand ??
                    (_cancelCommand = new RelayCommand(obj =>
                    {
                        _currentWindow.DialogResult = false;
                    }));
            }
        }

        public void CheckInput()
        {
            LoaderVisibility = Visibility.Visible;
            CreatingVisibility = Visibility.Hidden;
            using (ProjectContext db = new ProjectContext())
            {
                try
                {
                    if (String.IsNullOrEmpty(Login))
                    {
                        Errors = "Login is empty";
                    }
                    else if (db.Users.Where(p => p.Login == Login).Count() > 0)
                    {
                        Errors = "Such login is already exist";
                    }
                    else
                    {
                        _currentWindow.Dispatcher.Invoke(() => _currentWindow.DialogResult = true);
                    }
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    _logger.Error(ex.Message);
                    Errors = "Connection error";
                }
            }
            LoaderVisibility = Visibility.Hidden;
            CreatingVisibility = Visibility.Visible;
        }

        private string _errors;
        public string Errors
        {
            get => _errors;
            set
            {
                _errors = value;
                OnPropertyChanged(nameof(Errors));
            }
        }

        private string _login;
        public string Login
        {
            get => _login;
            set
            {
                _login = value;
                OnPropertyChanged(nameof(Login));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        public Visibility CreatingVisibility
        {
            get => _creatingVisibility;
            set
            {
                _creatingVisibility = value;
                OnPropertyChanged(nameof(CreatingVisibility));
            }
        }

        public Visibility LoaderVisibility
        {
            get => _loaderVisibility;
            set
            {
                _loaderVisibility = value;
                OnPropertyChanged(nameof(LoaderVisibility));
            }
        }
    }
}
