﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;
using Win10task1.View;
using System.Windows;
using System.Globalization;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System.Threading.Tasks;

namespace Win10task1
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private Window _currentMainWindow;
        private User _currentUser;
        private Process _currentProcess;
        private Issue _currentIssue;
        private ObservableCollection<Process> _processes;
        private ObservableCollection<Issue> _issues;
        private System.Windows.Media.Brush _issueColor;
        private Visibility _issuesPageVisibility;
        private Visibility _usersPageVisibility;
        private Visibility _issuePageVisibility;
        private bool _maintainerButtonsEnabled;
        private bool _workerButtonsEnabled;
        private ObservableCollection<User> _usersView;
        private string _selectedProcessName;
        private string _type;
        private string _selectedLanguage;
        private bool _processPageSelected;
        private bool _issuesPageSelected;
        private bool _usersPageSelected;
        private ProjectDbRepository _dbRepository;

        #region Binded properties

        public bool UsersPageSelected
        {
            get => _usersPageSelected;
            set
            {
                _usersPageSelected = value;
                OnPropertyChanged(nameof(UsersPageSelected));
            }
        }

        public bool IssuesPageSelected
        {
            get => _issuesPageSelected;
            set
            {
                _issuesPageSelected = value;
                OnPropertyChanged(nameof(IssuesPageSelected));
            }
        }

        public bool ProcessPageSelected
        {
            get => _processPageSelected;
            set
            {
                _processPageSelected = value;
                OnPropertyChanged(nameof(ProcessPageSelected));
            }
        }

        public Visibility IssuesPageVisibility
        {
            get => _issuesPageVisibility;
            set
            {
                _issuesPageVisibility = value;
                OnPropertyChanged(nameof(IssuesPageVisibility));
            }
        }

        public Visibility UsersPageVisibility
        {
            get => _usersPageVisibility;
            set
            {
                _usersPageVisibility = value;
                OnPropertyChanged(nameof(UsersPageVisibility));
            }
        }

        public Visibility IssuePageVisibility
        {
            get => _issuePageVisibility;
            set
            {
                _issuePageVisibility = value;
                OnPropertyChanged(nameof(IssuePageVisibility));
            }
        }

        public bool MaintainerButtonsEnabled
        {
            get => _maintainerButtonsEnabled;
            set
            {
                _maintainerButtonsEnabled = value;
                OnPropertyChanged(nameof(MaintainerButtonsEnabled));
            }
        }

        public bool WorkerButtonsEnabled
        {
            get => _workerButtonsEnabled;
            set
            {
                _workerButtonsEnabled = value;
                OnPropertyChanged(nameof(WorkerButtonsEnabled));
            }
        }

        public Process CurrentProcess
        {
            get => _currentProcess;
            set
            {
                _currentProcess = value;
                OnPropertyChanged(nameof(CurrentProcess));
            }
        }

        private User _selectedUser;
        public User SelectedUser
        {
            get => _selectedUser;
            set
            {
                _selectedUser = value;
                OnPropertyChanged(nameof(SelectedUser));
            }
        }

        public User CurrentUser
        {
            get => _currentUser;
            set
            {
                //_currentUser = value;
                OnPropertyChanged(nameof(CurrentUser));
            }
        }

        public Issue CurrentIssue
        {
            get => _currentIssue;
            set
            {
                _currentIssue = value;
                OnPropertyChanged(nameof(CurrentIssue));
            }
        }

        public ObservableCollection<User> UsersView
        {
            get => _usersView;
            set
            {
                _usersView = value;
                OnPropertyChanged(nameof(UsersView));
            }
        }

        public string Type
        {
            get => _type;
            set
            {
                _type = value;
                OnPropertyChanged(nameof(Type));
            }
        }

        public string SelectedProcessName
        {
            get => _selectedProcessName;
            set
            {
                _selectedProcessName = value;
                OnPropertyChanged(nameof(SelectedProcessName));
            }
        }

        public string SelectedLanguage
        {
            get => _selectedLanguage;
            set
            {
                _selectedLanguage = value;
                OnPropertyChanged(nameof(SelectedLanguage));
                if (_selectedLanguage == "Русский")
                {
                    App.Language = new CultureInfo("ru-RU");
                }
                else if (_selectedLanguage == "English")
                {
                    App.Language = new CultureInfo("en-US");
                }
            }
        }

        private void LanguageChanged(Object sender, EventArgs e)
        {
            CultureInfo currLang = App.Language;
        }

        public System.Windows.Media.Brush IssueColor
        {
            get => _issueColor;
            set
            {
                _issueColor = value;
                OnPropertyChanged(nameof(IssueColor));
            }
        }

        public ObservableCollection<Process> Processes
        {
            get => _processes;
            private set
            {
                if (value == _processes) return;
                _processes = value;
                OnPropertyChanged(nameof(Processes));
            }
        }

        public ObservableCollection<Issue> Issues
        {
            get => _issues;
            private set
            {
                if (value == _issues) return;
                _issues = value;
                OnPropertyChanged(nameof(Issues));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        #endregion

        public MainWindowViewModel(Window mWindow)
        {
            _processes = new ObservableCollection<Process>();
            _issues = new ObservableCollection<Issue>();
            _usersView = new ObservableCollection<User>();
            _dbRepository = new ProjectDbRepository();
            _currentMainWindow = mWindow;
            MaintainerButtonsEnabled = false;
            WorkerButtonsEnabled = false;
            IssueColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Red);
            App.LanguageChanged += LanguageChanged;
        }

        //Login or creating new account
        public async void Authorization()
        {
            LoginWindow loginWindow = new LoginWindow();
            loginWindow.Owner = _currentMainWindow;
            if (loginWindow.ShowDialog() == true)
            {
                //if flag create accaunt in login windos is true - creating account
                if (loginWindow.createNewAcc)
                {
                    CreateUserWindow createUserWindow = new CreateUserWindow();
                    createUserWindow.Owner = _currentMainWindow;
                    if (createUserWindow.ShowDialog() == true)
                    {
                        _currentUser = new User(createUserWindow.tbx_UserLogin.Text, createUserWindow.tbx_UserName.Text,
                            createUserWindow.tbx_UserSurname.Text, createUserWindow.pbx_UserPassword.Password);
                        await _dbRepository.CreateAsync(_currentUser);
                        //getting Id of user
                        _currentUser = await _dbRepository.GetUserByLoginAsync(_currentUser.Login);
                    }
                    //if creating of account was canceld - launch authorisation again
                    else
                    {
                        Authorization();
                    }
                }
                //finding user in data base
                else
                {
                    _currentUser = await _dbRepository.GetUserByLoginAsync(loginWindow.tbx_UserLogin.Text);
                    ProcessesInitialization();
                }
                //set user for view
                CurrentUser = _currentUser;
                Type = "User";
            }
            else
            {
                Application.Current.Shutdown();
            }
        }

        //finding processes that conneted to user and adding them on view
        public async void ProcessesInitialization()
        {
            foreach(Process p in await _dbRepository.GetUserProcessesAsync(_currentUser))
            {
                Processes.Add(p);
            }
        }

        public async Task<bool> IsWorker(User user)
        {
            if (await _dbRepository.GetUserTypeAsync(_currentProcess, user) == "Worker")
            {
                return true;
            }
            return false;
        }

        public async Task<bool> IsGuest(User user)
        {
            if (await _dbRepository.GetUserTypeAsync(_currentProcess, user) == "Guest")
            {
                return true;
            }
            return false;
        }

        public async Task<bool> IsCreator(User user)
        {
            if (await _dbRepository.GetUserTypeAsync(_currentProcess, user) == "User")
            {
                return true;
            }
            return false;
        }

        //show all users that connected to the selected process
        public async Task AddUsers()
        {
            foreach (User u in await _dbRepository.GetProcessUsersAsync(_currentProcess))
            {
                _currentMainWindow.Dispatcher.Invoke(() => UsersView.Add(u));
            }
        }

        public async Task AddIssues()
        {
            foreach (Issue i in await _dbRepository.GetProcessIssuesAsync(_currentProcess))
            {
                _currentMainWindow.Dispatcher.Invoke(() => Issues.Add(i));
            }
        }

        public async Task CreateNewMaintainer(User newUser)
        {
            Maintainer newMaintainer = new Maintainer(newUser.Login, newUser.Name, newUser.Surname, newUser.Password);
            await _dbRepository.AddUserAsync(newMaintainer, _currentProcess);
            _currentMainWindow.Dispatcher.Invoke(() => UsersView.Add(newMaintainer));
        }

        public async Task CreateNewWorker(User newUser)
        {
            Worker newWorker = new Worker(newUser.Login, newUser.Name, newUser.Surname, newUser.Password);
            await _dbRepository.AddUserAsync(newWorker, _currentProcess);
            _currentMainWindow.Dispatcher.Invoke(() => UsersView.Add(newWorker));
        }

        public async Task CreateNewGuest(User newUser)
        {
            Guest newGuest = new Guest(newUser.Login, newUser.Name, newUser.Surname, newUser.Password);
            await _dbRepository.AddUserAsync(newGuest, _currentProcess);
            _currentMainWindow.Dispatcher.Invoke(() => UsersView.Add(newGuest));
        }

        public int GetPriorityNumber(string priority)
        {
            if(priority.Equals("High"))
            {
                return (int)PriorityNumbers.High;
            }
            else if (priority.Equals("Medium"))
            {
                return (int)PriorityNumbers.Medium;
            }
            else
            {
                return (int)PriorityNumbers.Low;
            }
        }

        public int GetStatusNumber(string status)
        {
            if (status.Equals("To Do"))
            {
                return (int)StatusNumbers.ToDo;
            }
            else if (status.Equals("In Progress"))
            {
                return (int)StatusNumbers.InProgress;
            }
            else if (status.Equals("In Review"))
            {
                return (int)StatusNumbers.InReview;
            }
            else 
            {
                return (int)StatusNumbers.Done;
            }
        }

        public string GetPriorityString(int priority)
        {
            if (priority == 0)
            {
                return "High";
            }
            else if (priority == 1)
            {
                return "Medium";
            }
            else
            {
                return "Low";
            }
        }

        public string GetStatusString(int status)
        {
            if (status == 0)
            {
                return "To Do";
            }
            else if (status == 1)
            {
                return "In Progress";
            }
            else if (status == 2)
            {
                return "In Review";
            }
            else 
            {
                return "Done";
            }
        }

        public async void ShowError(string error)
        {
            var metroWindow = (Application.Current.MainWindow as MetroWindow);
            await metroWindow.ShowMessageAsync("Error", error);
        }

        #region Commands

        #region Process Commands

        private MVVM.RelayCommand _createProcessCommand;
        public MVVM.RelayCommand CreateProcessCommand
        {
            get
            {
                return _createProcessCommand ??
                  (_createProcessCommand = new MVVM.RelayCommand(async (obj) =>
                  {
                      CreateProcessWindow createProcessWindow = new CreateProcessWindow();
                      createProcessWindow.Owner = _currentMainWindow;
                      if(createProcessWindow.ShowDialog() == true)
                      {
                          Process process = new Process(createProcessWindow.tbx_ProcessName.Text, DateTime.Now, _currentUser);
                          await _dbRepository.AddProcessAsync(_currentUser, process);
                          Processes.Add(process);
                          ProcessPageSelected = true;
                      }
                  }));
            }
        }

        private MVVM.RelayCommand _selectProcessCommand;
        public MVVM.RelayCommand SelectProcessCommand
        {
            get
            {
                return _selectProcessCommand ??
                  (_selectProcessCommand = new MVVM.RelayCommand(async (obj) =>
                  {
                      if(_currentProcess != null)
                      {
                          CurrentProcess = _currentProcess;
                          IssuesPageVisibility = Visibility.Visible;
                          //chek the type and enable rights of current user for selected process
                           if (await IsWorker(_currentUser))
                           {
                               WorkerButtonsEnabled = true;
                               MaintainerButtonsEnabled = false;
                               UsersPageVisibility = Visibility.Hidden;
                               Type = "Worker";
                           }
                           else if (await IsGuest(_currentUser))
                           {
                               MaintainerButtonsEnabled = false;
                               WorkerButtonsEnabled = false;
                               UsersPageVisibility = Visibility.Hidden;
                               Type = "Guest";
                           }
                           else if (await IsCreator(_currentUser))
                           {
                               MaintainerButtonsEnabled = true;
                               WorkerButtonsEnabled = true;
                               UsersPageVisibility = Visibility.Visible;
                               Type = "User";
                               CurrentUser = _currentUser as Maintainer;
                           }
                           else
                           {
                               MaintainerButtonsEnabled = true;
                               WorkerButtonsEnabled = true;
                               UsersPageVisibility = Visibility.Visible;
                               Type = "Maintainer";
                               CurrentUser = _currentUser as Maintainer;
                           }
                          SelectedProcessName = CurrentProcess.Name;
                          Issues.Clear();
                          UsersView.Clear();
                          await AddUsers();
                          await AddIssues();
                          IssuesPageSelected = true;
                          IssuePageVisibility = Visibility.Hidden;
                      }
                  }));
            }
        }

        private MVVM.RelayCommand _deleteProcessCommand;
        public MVVM.RelayCommand DeleteProcessCommand
        {
            get
            {
                return _deleteProcessCommand ??
                  (_deleteProcessCommand = new MVVM.RelayCommand(async (obj) =>
                  {
                      if (_currentProcess != null)
                      {
                          if (await IsCreator(_currentUser))
                          {
                              _currentProcess = obj as Process;
                              await _dbRepository.DeleteAsync(_currentProcess);
                              Issues.Clear();
                              UsersView.Clear();
                              Processes.Remove(_currentProcess);
                              IssuesPageVisibility = Visibility.Hidden;
                              IssuePageVisibility = Visibility.Hidden;
                              UsersPageVisibility = Visibility.Hidden;
                              SelectedProcessName = null;
                              Type = null;
                              MaintainerButtonsEnabled = false;
                              WorkerButtonsEnabled = false;
                          }
                          else
                          {
                              ShowError("You have no rights to remove this process");
                          }
                      }
                  }));
            }
        }

        #endregion

        #region Issue Commands 

        private MVVM.RelayCommand _addIssueCommand;
        public MVVM.RelayCommand AddIssueCommand
        {
            get
            {
                return _addIssueCommand ??
                  (_addIssueCommand = new MVVM.RelayCommand(async (obj) =>
                  {
                      AddIssueWindow isuueWindow = new AddIssueWindow();
                      isuueWindow.Owner = _currentMainWindow;
                      if (isuueWindow.ShowDialog() == true)
                      {
                          Issue issue = new Issue(isuueWindow.tbx_IssueName.Text, DateTime.Now, isuueWindow.cmbx_PriorityList.Text, 
                              isuueWindow.cmbx_StatusList.Text, isuueWindow.tbx_IssueDescription.Text);
                          await _dbRepository.AddIssueAsync(issue, _currentProcess);
                          Issues.Add(issue);
                          IssuesPageSelected = true;
                      }
                  }));
            }
        }

        private MVVM.RelayCommand _selectIssueCommand;
        public MVVM.RelayCommand SelectIssueCommand
        {
            get
            {
                return _selectIssueCommand ??
                  (_selectIssueCommand = new MVVM.RelayCommand(obj =>
                  {
                      CurrentIssue = obj as Issue;
                      IssuePageVisibility = Visibility.Visible;
                  }));
            }
        }

        private MVVM.RelayCommand _deleteIssueCommand;
        public MVVM.RelayCommand DeleteIssueCommand
        {
            get
            {
                return _deleteIssueCommand ??
                  (_deleteIssueCommand = new MVVM.RelayCommand(async (obj) =>
                  {
                      ConfirmWindow confirmWindow = new ConfirmWindow();
                      confirmWindow.Owner = _currentMainWindow;
                      if(confirmWindow.ShowDialog() == true)
                      {
                          IssuePageVisibility = Visibility.Hidden;
                          Issues.Remove(CurrentIssue);
                          await _dbRepository.DeleteAsync(CurrentIssue);
                      }
                  }));
            }
        }

        private MVVM.RelayCommand _editIssueCommand;
        public MVVM.RelayCommand EditIssueCommand
        {
            get
            {
                return _editIssueCommand ??
                  (_editIssueCommand = new MVVM.RelayCommand(async (obj) =>
                  {
                      AddIssueWindow editIssueWindow = new AddIssueWindow();
                      editIssueWindow.Owner = _currentMainWindow;
                      editIssueWindow.tbx_IssueName.Text = CurrentIssue.Name;
                      editIssueWindow.cmbx_PriorityList.Text = CurrentIssue.Priority;
                      editIssueWindow.cmbx_StatusList.Text = CurrentIssue.Status;
                      editIssueWindow.tbx_IssueDescription.Text = CurrentIssue.Description;
                      if (editIssueWindow.ShowDialog() == true)
                      {
                          CurrentIssue.Name = editIssueWindow.tbx_IssueName.Text;
                          CurrentIssue.Priority = editIssueWindow.cmbx_PriorityList.Text;
                          CurrentIssue.Status = editIssueWindow.cmbx_StatusList.Text;
                          CurrentIssue.Description = editIssueWindow.tbx_IssueDescription.Text;
                          await _dbRepository.UpdateAsync(CurrentIssue);
                      }
                  }));
            }
        }

        #endregion

        #region User Commands

        private MVVM.RelayCommand _deleteUserCommand;
        public MVVM.RelayCommand DeleteUserCommand
        {
            get
            {
                return _deleteUserCommand ??
                  (_deleteUserCommand = new MVVM.RelayCommand(async (obj) =>
                  {
                      //if removable user is creator of selected process - show message
                      User u = obj as User;
                      if (!u.GetType().IsSubclassOf(typeof(User)))
                      {
                          ShowError("You can't delete creator!");
                      }
                      else if (u.Login == _currentUser.Login)
                      {
                          ShowError("You can't delete yourself!");
                      }
                      else
                      {
                          UsersView.Remove(u);
                          await _dbRepository.DeleteAsync(u);
                      }
                  }));
            }
        }

        private MVVM.RelayCommand _changeUserTypeCommand;
        public MVVM.RelayCommand ChangeUserTypeCommand
        {
            get
            {
                return _changeUserTypeCommand ??
                  (_changeUserTypeCommand = new MVVM.RelayCommand(async (obj) =>
                  {
                      User u = obj as User;
                      if (!u.GetType().IsSubclassOf(typeof(User)))
                      {
                          ShowError("You can't change type of creator!");
                      }
                      else if(u.Login == _currentUser.Login)
                      {
                          ShowError("You can't change your type!");
                      }
                      else
                      {
                          ChangeUserTypeWindow changeUsWind = new ChangeUserTypeWindow();
                          changeUsWind.Owner = _currentMainWindow;
                          //deleting old type from data base and creating new type of changeable user
                          if (changeUsWind.ShowDialog() == true)
                          {
                              if (changeUsWind.UserList.Text == "Maintainer")
                              {
                                  await CreateNewMaintainer(u);
                              }
                              else if (changeUsWind.UserList.Text == "Worker")
                              {
                                  await CreateNewWorker(u);
                              }
                              else
                              {
                                  await CreateNewGuest(u);
                              }
                              UsersView.Remove(u);
                              await _dbRepository.DeleteAsync(u);
                          }
                      }
                  }));
            }
        }

        private MVVM.RelayCommand _addUserCommand;
        public MVVM.RelayCommand AddUserCommand
        {
            get
            {
                return _addUserCommand ??
                  (_addUserCommand = new MVVM.RelayCommand(async (obj) =>
                  {
                      AddUserWindow userWindow = new AddUserWindow(_usersView.ToList());
                      userWindow.Owner = _currentMainWindow;
                      if (userWindow.ShowDialog() == true)
                      {
                          User newUser;
                          newUser = await _dbRepository.GetUserByLoginAsync(userWindow.tbx_UserLogin.Text);
                          if (userWindow.cmbx_UserList.Text == "Maintainer")
                          {
                              await CreateNewMaintainer(newUser);
                          }
                          else if (userWindow.cmbx_UserList.Text == "Worker")
                          {
                              await CreateNewWorker(newUser);
                          }
                          else
                          {
                              await CreateNewGuest(newUser);
                          }
                          UsersPageSelected = true;
                      }
                  }));
            }
        }

        #endregion

        private MVVM.RelayCommand _loadedCommand;
        public MVVM.RelayCommand LoadedCommand
        {
            get
            {
                return _loadedCommand ??
                  (_loadedCommand = new MVVM.RelayCommand((obj) =>
                  {
                      ProcessPageSelected = true;
                      UsersPageVisibility = Visibility.Hidden;
                      IssuesPageVisibility = Visibility.Hidden;
                      IssuePageVisibility = Visibility.Hidden;
                      Authorization();
                  }));
            }
        }

        private MVVM.RelayCommand _logOutCommand;
        public MVVM.RelayCommand LogOutCommand
        {
            get
            {
                return _logOutCommand ??
                  (_logOutCommand = new MVVM.RelayCommand(obj =>
                  {
                      ProcessPageSelected = true;
                      Processes.Clear();
                      IssuesPageVisibility = Visibility.Hidden;
                      UsersPageVisibility = Visibility.Hidden;
                      IssuePageVisibility = Visibility.Hidden;
                      MaintainerButtonsEnabled = false;
                      WorkerButtonsEnabled = false;
                      SelectedProcessName = "";
                      Type = "";
                      Authorization();
                  }));
            }
        }

        #endregion

    }
}
