﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using NLog;
using MVVM;
using System.Threading.Tasks;

namespace Win10task1
{
    public class LoginWindowViewModel : INotifyPropertyChanged
    {
        private Window _currentWindow;
        private Visibility _loggerVisibility;
        private Visibility _loaderVisibility;
        private string _password;
        private Logger _logger;

        public LoginWindowViewModel(Window window)
        {
            _currentWindow = window;
            _logger = LogManager.GetCurrentClassLogger();
            LoaderVisibility = Visibility.Hidden;
            LoggerVisibility = Visibility.Visible;
        }

        private RelayCommand _loginCommand;
        public RelayCommand LoginCommand
        {
            get
            {
                return _loginCommand ??
                    (_loginCommand = new RelayCommand(async (obj) =>
                    {
                        PasswordBox passwordBox = obj as PasswordBox;
                        _password = passwordBox.Password;
                        await Task.Run(() =>
                        {
                            CheckInput();
                        });
                    }));
            }
        }

        public void CheckInput()
        {
            LoaderVisibility = Visibility.Visible;
            LoggerVisibility = Visibility.Hidden;
            using (ProjectContext db = new ProjectContext())
            {
                try
                {
                    if (String.IsNullOrEmpty(Login))
                    {
                        Errors = "Login is empty";
                    }
                    else if (db.Users.Where(p => p.Login == Login).Count() != 0)
                    {
                        if (db.Users.Where(p => p.Login == Login).ToList().First().Password == _password)
                        {
                            _currentWindow.Dispatcher.Invoke(() => _currentWindow.DialogResult = true);
                        }
                        else
                        {
                            Errors = "Wrong password";
                        }
                    }
                    else
                    {
                        Errors = "No such user";
                    }
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    _logger.Error(ex.Message);
                    Errors = "Connection error";
                }
            }
            LoaderVisibility = Visibility.Hidden;
            LoggerVisibility = Visibility.Visible;
        }

        private string _errors;
        public string Errors
        {
            get => _errors;
            set
            {
                _errors = value;
                OnPropertyChanged(nameof(Errors));
            }
        }

        private string _login;
        public string Login
        {
            get => _login;
            set
            {
                _login = value;
                OnPropertyChanged(nameof(Login));
            }
        }

        public Visibility LoggerVisibility
        {
            get => _loggerVisibility;
            set
            {
                _loggerVisibility = value;
                OnPropertyChanged(nameof(LoggerVisibility));
            }
        }

        public Visibility LoaderVisibility
        {
            get => _loaderVisibility;
            set
            {
                _loaderVisibility = value;
                OnPropertyChanged(nameof(LoaderVisibility));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

    }
}
