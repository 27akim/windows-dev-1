﻿using System.Data.Entity;

namespace Win10task1
{
    public class ProjectContext : DbContext
    {
        public ProjectContext() : base("Project")
        {

        }
        public DbSet<User> Users { get; set; }
        public DbSet<Maintainer> Maintainers { get; set; }
        public DbSet<Worker> Workers { get; set; }
        public DbSet<Guest> Guests { get; set; }
        public DbSet<Process> Processes { get; set; }
        public DbSet<Issue> Issues { get; set; }
    }
}
